﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public abstract class BaseParser<T> : IDataParser<T>
    {
        protected readonly string dataFile;
        public BaseParser(string dataFile) => this.dataFile = dataFile;

        public abstract T Parse();

    }

}
