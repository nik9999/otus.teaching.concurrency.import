﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : BaseParser<List<Customer>>
    {
        public CsvParser(string dataFile) : base(dataFile) { }
        public override List<Customer> Parse()
        {
            try
            {
                var customers = CsvSerializer.DeserializeFromReader<List<Customer>>(File.OpenText(dataFile));

                return customers;
            }

            catch
            {
                return new List<Customer>();
            }

        }
    }
}
