﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : BaseParser<List<Customer>>
    {
        public XmlParser(string dataFile) : base(dataFile) { }
        public override List<Customer> Parse() 
    {
            try
            {
                using var stream = new FileStream(dataFile, FileMode.Open);
                var customers = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(stream);

                return customers?.Customers;
            }

            catch
            {
                return new List<Customer>();
            }

        }

    }
}