﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class SQLiteRepository : ICustomerRepository
    {
        private int retryCount = 3;

        public SQLiteRepository()
        {
            using (var db = new CustomerContext())
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
            }
        }
        public void AddCustomer(Customer customer)
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    using (var db = new CustomerContext())
                    {
                        db.Add(customer);
                        db.SaveChanges();
                    }

                    break;
                }
                catch
                {
                    currentRetry++;
                    if (currentRetry > this.retryCount)
                    {
                        throw;
                    }
                }

            }
        }
    }
}
