﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// https://docs.microsoft.com/en-us/azure/architecture/patterns/retry

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class TxtFileRepository : ICustomerRepository
    {
        private int retryCount = 3;

        private const string _fileName = "customers.txt";

        public TxtFileRepository()
        {
            File.WriteAllText(_fileName, String.Empty);
        }

        public void AddCustomer(Customer customer)
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    string s = $"{customer.FullName} {customer.Phone} {customer.Email}{System.Environment.NewLine}";
                    File.AppendAllText(_fileName, s);

                    break;
                }
                catch 
                {
                    currentRetry++;
                    if (currentRetry > this.retryCount)
                    {
                        throw;
                    }
                }
            }

        }
    }
}
