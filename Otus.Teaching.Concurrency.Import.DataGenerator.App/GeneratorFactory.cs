using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.IO;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount)
        {
            if (Path.GetExtension(fileName).Replace(".", "").ToUpper() == "XML")
                return new XmlDataGenerator(fileName, dataCount);
            else
                return new CsvDataGenerator(fileName, dataCount); 
        }
    }
}