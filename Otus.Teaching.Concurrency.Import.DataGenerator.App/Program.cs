﻿using Microsoft.Extensions.CommandLineUtils;
using System;
using System.IO;
using System.Reflection;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName = Path.Combine(_dataFileDirectory, "customers.xml"); 
        private static int _dataCount = 1000;

        static void Main(string[] args)
        {
            var app = new CommandLineApplication();

            var fileNameOption = app.Option("-f|--file <filename>",
                   "File name",
                   CommandOptionType.SingleValue);

            var generatorOption = app.Option("-g|--generator <xml_or_csv>",
                   "XML or CSV File generation",
                   CommandOptionType.SingleValue);

            var dataCountOption = app.Option("-d|--datacount <int>",
                   "Numbers of data",
                   CommandOptionType.SingleValue);

            app.OnExecute(() =>
            {

                if (fileNameOption.HasValue())
                {
                    if (generatorOption.HasValue())
                        _dataFileName = Path.Combine(_dataFileDirectory, fileNameOption.Value() + $".{generatorOption.Value()}");
                    else
                        _dataFileName = Path.Combine(_dataFileDirectory, fileNameOption.Value() + ".xml");
                }

                if (dataCountOption.HasValue())
                    int.TryParse(dataCountOption.Value(), out _dataCount);

                Console.WriteLine($"Generating {Path.GetExtension(_dataFileName).Replace(".", "").ToUpper()} data...");

                var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount);

                generator.Generate();

                Console.WriteLine($"Generated {Path.GetExtension(_dataFileName).Replace(".", "").ToUpper()} data in {Path.GetFileName(_dataFileName)}...");


                return 0;
            });

            try
            {
                Console.WriteLine("DataGenerator app executing...");
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine($"CommandParsingException: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to execute application: {0}", ex.Message);
            }

        }
    }
}