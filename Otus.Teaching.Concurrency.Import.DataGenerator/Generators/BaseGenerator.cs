﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public abstract class BaseGenerator : IDataGenerator
    {
        protected readonly string _fileName;
        protected readonly int _dataCount;

        public BaseGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public abstract void Generate();
    }
}
