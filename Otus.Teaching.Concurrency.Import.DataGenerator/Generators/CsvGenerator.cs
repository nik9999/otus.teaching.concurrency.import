﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : BaseGenerator
    {
        public CsvGenerator(string fileName, int dataCount) : base(fileName, dataCount) { }

        public override void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var contents = CsvSerializer.SerializeToCsv(customers);
            File.WriteAllText(_fileName, contents);
        }

    }
}
