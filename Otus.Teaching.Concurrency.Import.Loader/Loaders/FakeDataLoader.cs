using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


//https://stackoverflow.com/questions/2538065/what-is-the-basic-concept-behind-waithandle/2538308

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader : IDataLoader
    {
        private object lockObject = new object();

        private List<List<Customer>> subCollections;
        private readonly bool usingThreadPoll;
        private readonly int threadCount;
        private readonly bool usingSqlite;

        private ICustomerRepository repo;

        public FakeDataLoader(List<Customer> customers, bool usingThreadPoll, int threadCount, bool usingSqlite)
        {
            this.usingThreadPoll = usingThreadPoll;
            this.threadCount = threadCount;
            this.usingSqlite = usingSqlite;

            var subCollectionsCount = customers.Count / threadCount;

            subCollections = new List<List<Customer>>();

            for (int i = 0; i < threadCount; i++)
                subCollections.Add(customers.GetRange(i * subCollectionsCount, subCollectionsCount));

            //��������� ������� �� ������� 
            subCollections.Last().AddRange(customers.GetRange(customers.Count - customers.Count % threadCount, customers.Count % threadCount));

        }
        public void LoadData()
        {
            if (usingSqlite)
                repo = new SQLiteRepository();
            else
                repo = new TxtFileRepository();

            ManualResetEvent[] events = new ManualResetEvent[threadCount];

            for (int i = 0; i < threadCount; i++)
            {
                events[i] = new ManualResetEvent(false);
                var threadData = Tuple.Create(events[i], i + 1, subCollections.ElementAt(i)); //ManualResetEvent, threadId, subCollection
                if (usingThreadPoll)
                    ThreadPool.QueueUserWorkItem(HandleItem, threadData);
                else
                    new Thread(new ParameterizedThreadStart(HandleItem)).Start(threadData);
            }

            WaitHandle.WaitAll(events);

            Console.WriteLine("All finished!");
        }

        void HandleItem(object data)
        {
            Console.WriteLine($"Thread Id {(data as Tuple<ManualResetEvent, int, List<Customer>>).Item2} started!");

            foreach (var cust in (data as Tuple<ManualResetEvent, int, List<Customer>>).Item3)
                lock (lockObject) { repo.AddCustomer(cust); }

            Console.WriteLine($"Thread Id {(data as Tuple<ManualResetEvent, int, List<Customer>>).Item2} finished!");

            (data as Tuple<ManualResetEvent, int, List<Customer>>).Item1.Set();
        }
    }
}