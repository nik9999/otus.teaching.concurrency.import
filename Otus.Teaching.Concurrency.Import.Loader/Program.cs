﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.CommandLineUtils;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName = Path.Combine(_dataFileDirectory, "customers.xml");
        private static int _dataCount = 1000;
        private static bool _externalGenerator = false;
        private static bool _usingThreadPoll = false;
        private static int _threadCount = 3;
        private static bool _usingSQLite = false;

        private static readonly string _externalGeneratorFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("Loader", "DataGenerator.App"), "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");

        static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var app = new CommandLineApplication();

            var fileNameOption = app.Option("-f|--file <filename>",
                   "File name",
                   CommandOptionType.SingleValue);

            var generatorOption = app.Option("-g|--generator <xml_or_csv>",
                   "XML or CSV File generation",
                   CommandOptionType.SingleValue);

            var dataCountOption = app.Option("-d|--datacount <int>",
                   "Numbers of data",
                   CommandOptionType.SingleValue);

            var externalGeneratorOption = app.Option("-e|--exgen",
                    "Geaneratin data internal method.",
                    CommandOptionType.NoValue);
            
            var usingThreadPollOption = app.Option("-p|--poolusing",
                   "Using ThreadPoll",
                   CommandOptionType.NoValue);

            var threadCountOption = app.Option("-t|--threadcount <int>",
                   "Threads count",
                   CommandOptionType.SingleValue);

            var usingSQLiteOption = app.Option("-s|--sqlite",
                  "Using SQLite , no option txt file",
                  CommandOptionType.NoValue);

            app.OnExecute(() =>
            {

                if (fileNameOption.HasValue())
                {
                    if (generatorOption.HasValue())
                        _dataFileName = Path.Combine(_dataFileDirectory, fileNameOption.Value() + $".{generatorOption.Value()}");
                    else
                        _dataFileName = Path.Combine(_dataFileDirectory, fileNameOption.Value() + ".xml");
                }

                if (dataCountOption.HasValue())
                    int.TryParse(dataCountOption.Value(), out _dataCount);

                if (externalGeneratorOption.HasValue())
                    _externalGenerator = true;

                if (usingThreadPollOption.HasValue())
                    _usingThreadPoll = true;

                if (threadCountOption.HasValue())
                    int.TryParse(threadCountOption.Value(), out _threadCount);

                if (usingSQLiteOption.HasValue())
                    _usingSQLite = true;

                // work

                var stopWatch = new Stopwatch();
                stopWatch.Start();

                if (_externalGenerator)
                {
                    Process process = new Process();
                    // Configure the process using the StartInfo properties.
                    process.StartInfo.FileName = _externalGeneratorFile;
                    process.StartInfo.Arguments = $"-f {Path.GetFileNameWithoutExtension(_dataFileName)} -g {Path.GetExtension(_dataFileName).Replace(".", "").ToLower()} -d {_dataCount}";

                    process.Start();
                    process.WaitForExit();

                }
                else
                    GenerateCustomersDataFile();

                IDataParser<List<Customer>> parser;
            
                if (Path.GetExtension(_dataFileName).Replace(".", "").ToLower() == "xml")
                    parser = new XmlParser(_dataFileName);
                else
                    parser = new CsvParser(_dataFileName);

                var customers = parser.Parse();

                var loader = new FakeDataLoader(parser.Parse(), _usingThreadPoll, _threadCount, _usingSQLite);

                loader.LoadData();

                Console.WriteLine($"Loading data for {_dataFileName} in {stopWatch.Elapsed} with {_threadCount} threads...");
                return 0;
            });

            try
            {
                Console.WriteLine("Data Loader app executing...");
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine($"CommandParsingException: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to execute application: {0}", ex.Message);
            }



            ////if (args != null && args.Length == 1)
            ////{
            ////    _dataFilePath = args[0];
            ////}

            ////Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            ////var stopWatch = new Stopwatch();
            ////stopWatch.Start();

            ////GenerateCustomersDataFile();

            ////var loader = new FakeDataLoader();

            ////loader.LoadData();

            ////Console.WriteLine($"Loading data for {_dataFilePath} in {stopWatch.Elapsed}...");
        }

        static void GenerateCustomersDataFile()
        {
            IDataGenerator generator;
            if (Path.GetExtension(_dataFileName).Replace(".", "").ToUpper() == "XML")
                generator = new XmlGenerator(_dataFileName, _dataCount);
            else
                generator = new CsvGenerator(_dataFileName, _dataCount);
            
            generator.Generate();
        }
    }
}