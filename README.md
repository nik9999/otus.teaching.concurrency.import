# Otus.Teaching.Concurrency.Import

## Usage

Otus.Teaching.Concurrency.Import.Loader.exe [option]

# -f|--file <filename>

# -g|--generator <xml_or_csv>

# -d|--datacount <int>
Default 1000

# -e|--exgen 
Geanerate data external method.

# -p|--poolusing 
Using ThreadPoll // default Thread

# -t|--threadcount <int>
default 3

# -s|--sqlite
Using SQLite // default txt file

## Example 
Otus.Teaching.Concurrency.Import.Loader.exe -f my_customers -g csv -d 1000 -p -t 3 -s -e


# Output
Loader started with process Id 36976...

Data Loader app executing...

DataGenerator app executing...

Generating CSV data...

Generated CSV data in my_customers.csv...

Thread Id 2 started!

Thread Id 3 started!

Thread Id 3 finished!

Thread Id 2 finished!

Thread Id 1 started!

Thread Id 1 finished!

All finished!

Loading data for D:\OTUS\C#\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\net6.0\my_customers.csv in 00:00:02.0582995 with 3 threads...
